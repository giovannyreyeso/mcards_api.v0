'use strict';
const express = require('express');
const fs = require('fs');
const router = express.Router();
const mainDirectory = `${__dirname}/api`;
function loadRoutes(folders) {
	for (let i = 0; i < folders.length; i++) {
		let element = folders[i];
		let files = fs.readdirSync(`${mainDirectory}/${element}`);
		for (let j = 0; j < files.length; j++) {
			let file = files[j];
			let stat = fs.statSync(`${mainDirectory}/${element}/${file}`);
			if (stat.isFile()) {
				if (new RegExp(/\w+.routes.js/).test(file)) {
					const route = require(`${mainDirectory}/${element}/${file.slice(0, -3)}`);
					router.use(route);
					console.info(`[ROUTE ADD] - ${file}`);
				}
			}
		}
	}
}
const folders = fs.readdirSync(mainDirectory);
loadRoutes(folders);
module.exports = router;