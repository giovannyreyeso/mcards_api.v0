'use strict';
const express = require('express');
const UserController = require('./user.controller');
const auth = require('../../middlewares/Auth');
const api = express.Router();
api.get('/me', auth.isAuthorized, UserController.Me);
api.get('/user', auth.isAuthorized, UserController.FinByEmail);
api.post('/register',UserController.Register);
module.exports = api;