const User = require('./models/User')
function FinByEmail(req, res) {
	User.find({
		'email': req.query.email
	}).select({ "name": 1, "_id": 1, "email": 1, "picture": 1 })
		.then(function (users) {
			return res.status(200).json(users);
		}).catch(function (err) {
			return res.status(500).json({
				statusCode: 500,
				message: err.message
			});
		});
}

function Me(req, res) {
	User.findOne({
		'uid': req.user.user_id
	}).then((user) => {
		res.status(200).json(user)
	}).catch((err) => {
		res.status(500).json(err)
	})
}

function Register(req, res) {
	User.findOne({
		'uid': req.user.user_id
	}).then((userSearched) => {
		if (userSearched) {
			throw "El usuario ya se encuentra registrado"
		}
		const user = new User({
			uid: req.user.user_id,
			name: req.user.name,
			email: req.user.email,
			picture: req.user.picture
		});

		return user.save();
	}).then((userSaved) => {
		res.status(200).json(userSaved)
	}).catch((err) => {
		res.status(500).json(err)
	});

}
module.exports = {
	Me,
	Register,
	FinByEmail
}
