'use strict';
const express = require('express');
const CardController = require('./cards.controller');
const MovementController = require('../movements/movements.controller');
const auth = require('../../middlewares/Auth');
const api = express.Router();
api.get('/x', (req, res) => {
    res.send("OK");
})
api.get('/card/:id', auth.isAuthorized, CardController.GetById);
api.get('/card', auth.isAuthorized, CardController.List);
api.post('/card', auth.isAuthorized, CardController.Create);
api.put('/card/:id', auth.isAuthorized, CardController.Modify);
api.delete('/card/:id', auth.isAuthorized, CardController.Delete);
api.post('/card/:id/shared/:date', auth.isAuthorized, CardController.SharedWith);
api.delete('/card/:id/shared/:iduser/', auth.isAuthorized, CardController.UndoShared);
api.get('/card/:id/purchase', auth.isAuthorized, CardController.Purchases)
api.get('/card/:id/purchase/nextmonth', auth.isAuthorized, CardController.PurchasesNextMonth)
api.get('/card/:id/purchase/month', auth.isAuthorized, CardController.PurchasesNow)
api.get('/card/:id/movement', auth.isAuthorized, MovementController.GetMovementByCard)
module.exports = api;
