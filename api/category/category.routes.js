'use strict';
const express = require('express');
const CategoryController = require('./category.controller');
const auth = require('../../middlewares/Auth');
const api = express.Router();

api.get('/category', auth.isAuthorized, CategoryController.List);
api.post('/category', auth.isAuthorized, CategoryController.Create);
api.put('/category', auth.isAuthorized, CategoryController.Modify);
api.delete('/category/:id', auth.isAuthorized, CategoryController.Delete);

module.exports = api;