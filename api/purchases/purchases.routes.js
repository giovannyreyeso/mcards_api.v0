'use strict';
const express = require('express');
const PurchaseController = require('./purchases.controller');
const auth = require('../../middlewares/Auth');
const api = express.Router();

api.get('/purchase/:id', auth.isAuthorized, PurchaseController.GetById)
api.get('/purchase', auth.isAuthorized, PurchaseController.List)
api.post('/purchase', auth.isAuthorized, PurchaseController.Create)
api.put('/purchase/:id', auth.isAuthorized, PurchaseController.Modify)
api.delete('/purchase/:id', auth.isAuthorized, PurchaseController.Delete);

module.exports = api;