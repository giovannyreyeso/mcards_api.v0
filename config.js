let defaultt = 'dev';
if (process.env.NODE_ENV) {
	defaultt = process.env.NODE_ENV;
}
console.log(`[LOADING API ON ${defaultt} CONF.]`);
function get() {
	const config = require('dotenv').config({
		path: `.env.${defaultt}`
	});
	return config.parsed;
}
module.exports = { get };
