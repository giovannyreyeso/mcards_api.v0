'use strict'
const config = require('./config');
const app = require('./app');
const mongoose = require('mongoose');
let firebase_admin = require("firebase-admin")
let bluebird = require("bluebird");
mongoose.Promise = bluebird;
const port = config.get().API_PORT;
const serviceAccount = require("./firebase/key.json");

app.listen(port, () => {
  console.log(`MCARDS API ready on port  ${port}`)
  firebase_admin.initializeApp({
    credential: firebase_admin.credential.cert(serviceAccount),
    databaseURL: "https://mcards-63925.firebaseio.com"
  });
})

mongoose.connect(config.get().DB_URL, {
  useMongoClient: true
});
const connection = mongoose.connection;
connection.once('open', function () {
  // we're connected!
  console.log("Conexion a la base de datos establecida")
});
connection.on('error', console.error.bind(console, 'connection error:'));