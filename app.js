'use strict'
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const api = require('./routes');
const morgan = require('morgan')

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan('combined'));
app.use('/api', api);
module.exports = app;
